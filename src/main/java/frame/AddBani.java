package frame;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.*;

import proc.Account;
import proc.Bank;

public class AddBani extends JFrame {

	private static final long serialVersionUID = 1L;

	private JLabel contNoLbl;
	private JLabel sumaLbl;
	
	private JTextField sumaTF;
	private JTextField contNoTF;

	private JButton addBtn;
	private JButton cancelBtn;

	private JPanel panel;
	
	
	public AddBani(final Bank banca) {
		
		panel = new JPanel();

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(335, 140);
		setTitle("Add money");

		panel.setLayout(null);
		
		add(panel);

		contNoLbl = new JLabel("Numar cont:");
		
		contNoLbl.setBounds(10, 10, 150, 20);
		panel.add(contNoLbl);
		
		contNoTF = new JTextField();
		contNoTF.setBounds(170, 10, 150, 20);
		panel.add(contNoTF);
		
		sumaLbl = new JLabel("Suma de bani:");
		
		sumaLbl.setBounds(10, 40, 150, 20);
		panel.add(sumaLbl);

		sumaTF = new JTextField();
		sumaTF.setBounds(170, 40, 150, 20);
		panel.add(sumaTF);

		addBtn = new JButton("Adauga bani");
	
		addBtn.setBounds(170, 70, 150, 30);
		panel.add(addBtn);

		cancelBtn = new JButton("Cancel");

		cancelBtn.setBounds(10, 70, 150, 30);
		panel.add(cancelBtn);

		addBtn.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent ae) {
				try {
					long contNo = Long.parseLong(contNoTF.getText());
					double suma = Double.parseDouble(sumaTF.getText());
					Account cont = banca.findAccount(contNo);
					if (cont.addMoney(suma)) {
						JOptionPane.showMessageDialog(null, "Money successfully added ", "Succes", JOptionPane.PLAIN_MESSAGE);
						
					} else {
						JOptionPane.showMessageDialog(null, "Error adding money ", "Error", JOptionPane.ERROR_MESSAGE);
					}
				
					banca.serializare();
					dispose();
					new OperatiiCont(banca);
				} catch (NumberFormatException e) {
					System.out.println("Problema input");
					
				} catch (NullPointerException e) {
					JOptionPane.showMessageDialog(null, "Account not found!", "Error", JOptionPane.ERROR_MESSAGE);
					
				} catch (IOException e) {
					System.out.println("Problema la serializare!");
					e.printStackTrace();
				} finally {
					dispose();
				}
			}
		});

		cancelBtn.addActionListener(new ActionListener() {
	
			public void actionPerformed(ActionEvent ae) {
				dispose();
				new Menu(banca);
			}
		});

		setResizable(false);
		setLocationRelativeTo(null);
		setVisible(true);
	}

}
