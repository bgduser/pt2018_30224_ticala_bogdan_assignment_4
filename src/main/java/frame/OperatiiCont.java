package frame;

import proc.Account;
import proc.Bank;
import proc.Person;

import java.awt.event.*;
import java.util.Set;
import java.util.Vector;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class OperatiiCont extends JFrame {
	private static final long serialVersionUID = 1L;



	private JButton addBtn;
	private JButton editBtn;
	private JButton delBtn;
	private JButton backBtn;

	private JTable contTable;

	private JPanel panel;



	private Bank banca;

	
	public OperatiiCont(final Bank banca) {
		
		this.banca = banca;
		
		panel = new JPanel();

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(655, 470);
		setTitle("Operatii cont");

		panel.setLayout(null);
		
		add(panel);

		addBtn = new JButton("Adauga cont nou");
		
		addBtn.setBounds(10, 10, 150, 30);
		panel.add(addBtn);

		editBtn = new JButton("Editare cont");
		
		editBtn.setBounds(170, 10, 150, 30);
		panel.add(editBtn);

		delBtn = new JButton("Stergere cont");
	
		delBtn.setBounds(330, 10, 150, 30);
		panel.add(delBtn);

		backBtn = new JButton("Back");
	
		backBtn.setBounds(490, 10, 150, 30);
		panel.add(backBtn);

		contTable = createTable();
		JScrollPane contSP = new JScrollPane(contTable);
		contSP.setBounds(10, 50, 630, 380);
		panel.add(contSP);

		
		backBtn.addActionListener(new ActionListener() {
		
			public void actionPerformed(ActionEvent ae) {
				dispose();
				new Menu(banca);
			}
		});

		addBtn.addActionListener(new ActionListener() {
		
			public void actionPerformed(ActionEvent ae) {
				dispose();
				new AddCont(banca);
			}
		});

		editBtn.addActionListener(new ActionListener() {
		
			public void actionPerformed(ActionEvent ae) {
				//dispose();
				new EditCont(banca);
			}
		});

		delBtn.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent ae) {
				dispose();
				new DelCont(banca);
			}
		});

		
		setResizable(false);
		setLocationRelativeTo(null);
		setVisible(true);
	}

	

}
