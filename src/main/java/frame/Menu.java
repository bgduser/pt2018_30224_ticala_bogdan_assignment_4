package frame;


import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.*;

import proc.Bank;

public class Menu extends JFrame {
	
private static final long serialVersionUID = 1L;
	
	
	
	private JPanel panel;

	private JButton persOpBtn;

	private JButton contOpBtn;


	private JButton adaugaBtn;

	private JButton retrageBtn;



	private JLabel titlu;



	private JLabel titlu2;

	
	public Menu(final Bank banca)  {
		
		
		
		panel = new JPanel();
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(375, 500);
		setTitle("Banca");
		
		panel.setLayout(null);
	
		add(panel);
		
		titlu = new JLabel("Proceduri banca");
		titlu.setBounds(10,10,350,45);
		panel.add(titlu);
		
		titlu2 = new JLabel("Proceduri persoana");
		titlu2.setBounds(10,180,350,45);
		panel.add(titlu2);
		
		persOpBtn = new JButton("Operatii persoana");

		persOpBtn.setBounds(10, 60, 350, 45);
		panel.add(persOpBtn);
		
		contOpBtn = new JButton("Operatii cont");
		
		contOpBtn.setBounds(10, 110, 350, 45);
		panel.add(contOpBtn);
		
	
		
		persOpBtn.addActionListener(new ActionListener() {
			
		

			public void actionPerformed(ActionEvent ae) {
				dispose();
				new OperatiiPersoana(banca);
			}
		});
		
		contOpBtn.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent ae) {
				dispose();
				new OperatiiCont(banca);
			}
		});
		
		
		
		
		adaugaBtn = new JButton("Adauga bani");
		
		adaugaBtn.setBounds(10, 230, 350, 45);
		panel.add(adaugaBtn);
		
		retrageBtn = new JButton("Retragere bani");
	
		retrageBtn.setBounds(10, 280, 350, 45);
		panel.add(retrageBtn);
	
		
		adaugaBtn.addActionListener(new ActionListener() {
		
			public void actionPerformed(ActionEvent ae) {
				dispose();
				new OperatiiCont(banca);
				new AddBani(banca);
			}
		});
		
		retrageBtn.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent ae) {
				dispose();
				new OperatiiCont(banca);
				new Withdraw(banca);
			}
		});
		
	
		
		setResizable(false);
		setLocationRelativeTo(null);
		setVisible(true);
	}
		
		

}
