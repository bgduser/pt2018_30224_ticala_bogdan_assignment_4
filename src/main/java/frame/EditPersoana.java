package frame;

import proc.Bank;
import proc.Person;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.*;

public class EditPersoana extends JFrame {
	
	private static final long serialVersionUID = 1L;

	private JLabel idLbl;
	private JLabel numeLbl;
	private JLabel telefonLbl;

	private JTextField idTF;
	private JTextField numeTF;
	private JTextField telefonTF;

	private JButton editBtn;
	private JButton cancelBtn;

	private JPanel panel;
	

	
	public EditPersoana(final Bank banca)  {
		panel = new JPanel();
	

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(335, 170);
		setTitle("Editare persoana");

		panel.setLayout(null);
	
		add(panel);

		idLbl = new JLabel("ID:");
	
		idLbl.setBounds(10, 10, 150, 20);
		panel.add(idLbl);

		idTF = new JTextField();
		idTF.setBounds(170, 10, 150, 20);
		panel.add(idTF);

		numeLbl = new JLabel("Nume nou:");
	
		numeLbl.setBounds(10, 40, 150, 20);
		panel.add(numeLbl);

		numeTF = new JTextField();
		numeTF.setBounds(170, 40, 150, 20);
		panel.add(numeTF);

		telefonLbl = new JLabel("Telefon nou:");
		
		telefonLbl.setBounds(10, 70, 150, 20);
		panel.add(telefonLbl);

		telefonTF = new JTextField();
		telefonTF.setBounds(170, 70, 150, 20);
		panel.add(telefonTF);

		editBtn = new JButton("Editare");
	
		editBtn.setBounds(170, 100, 150, 30);
		panel.add(editBtn);

		cancelBtn = new JButton("Cancel");
	
		cancelBtn.setBounds(10, 100, 150, 30);
		panel.add(cancelBtn);

		editBtn.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent ae) {
				try {
					long id = Long.parseLong(idTF.getText());
					Person oldPerson = banca.findPerson(id);
					Person newPerson = new Person();
					
					newPerson.setId(id);
					if (numeTF.getText().isEmpty()) {
						newPerson.setNume(oldPerson.getNume());
					} else {
						newPerson.setNume(numeTF.getText());
					}
					if (telefonTF.getText().isEmpty()) {
						newPerson.setTelefon(oldPerson.getTelefon());
					} else {
						newPerson.setTelefon(telefonTF.getText());
					}
					
					banca.updatePerson(oldPerson, newPerson);
					banca.serializare();

					JOptionPane.showMessageDialog(null, "Person successfully updated!", "Succes", JOptionPane.PLAIN_MESSAGE);
					new OperatiiPersoana(banca);
				} catch (NumberFormatException e) {
					System.out.println("Problema input");
					new OperatiiPersoana(banca);
				} catch (NullPointerException e) {
					JOptionPane.showMessageDialog(null, "Person not found!", "Error", JOptionPane.ERROR_MESSAGE);
					new OperatiiPersoana(banca);
				} catch (IOException e) {
				System.out.println("Problema la serializare!");
					e.printStackTrace();
				} finally {
					dispose();
				}
			}
		});

		cancelBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				dispose();
				new OperatiiPersoana(banca);
			}
		});

		setResizable(false);
		setLocationRelativeTo(null);
		setVisible(true);
	}

}
