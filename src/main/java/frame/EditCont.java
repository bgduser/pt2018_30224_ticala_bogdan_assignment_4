package frame;

import proc.Account;
import proc.Bank;
import proc.Person;
import proc.SavingAccount;
import proc.SpendingAccount;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.*;

public class EditCont extends JFrame {

	private static final long serialVersionUID = 1L;

	private JLabel personIdLbl;
	private JLabel numarContLbl;
	private JLabel newSumaLbl;
	private JLabel tipContLbl;

	private JTextField personIdTF;
	private JTextField numarContTF;
	private JTextField newSumaTF;
	
	@SuppressWarnings("rawtypes")
	private JComboBox accountTypeCB;

	private JButton editAccountBtn;
	private JButton cancelBtn;

	private JPanel panel;
	
	
	
	public EditCont(final Bank banca) {
		
		panel = new JPanel();

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(335, 200);
		setTitle("Editare cont");

		panel.setLayout(null);
	
		add(panel);

		personIdLbl = new JLabel("ID:");
		
		personIdLbl.setBounds(10, 10, 150, 20);
		panel.add(personIdLbl);

		personIdTF = new JTextField();
		personIdTF.setBounds(170, 10, 150, 20);
		panel.add(personIdTF);

		numarContLbl = new JLabel("Numar cont:");

		numarContLbl.setBounds(10, 40, 150, 20);
		panel.add(numarContLbl);

		numarContTF = new JTextField();
		numarContTF.setBounds(170, 40, 150, 20);
		panel.add(numarContTF);

		newSumaLbl = new JLabel("Suma noua:");
	
		newSumaLbl.setBounds(10, 70, 150, 20);
		panel.add(newSumaLbl);

		newSumaTF = new JTextField();
		newSumaTF.setBounds(170, 70, 150, 20);
		panel.add(newSumaTF);

		tipContLbl = new JLabel("Tip cont nou:");

		tipContLbl.setBounds(10, 100, 150, 20);
		panel.add(tipContLbl);
		
		String[] accountTypes = { "Spending account", "Saving account" };
		accountTypeCB = new JComboBox<Object>(accountTypes);
	
		accountTypeCB.setBounds(170, 100, 150, 20);
		panel.add(accountTypeCB);
		
		editAccountBtn = new JButton("Editare");
	
		editAccountBtn.setBounds(170, 130, 150, 30);
		panel.add(editAccountBtn);

		cancelBtn = new JButton("Cancel");
	
		cancelBtn.setBounds(10, 130, 150, 30);
		panel.add(cancelBtn);

		editAccountBtn.addActionListener(new ActionListener() {
	
			public void actionPerformed(ActionEvent ae) {
				try {
					long personId = Long.parseLong(personIdTF.getText());
					long numarCont = Long.parseLong(numarContTF.getText());
					Person person = banca.findPerson(personId);
					Account oldAccount = banca.findAccount(numarCont);
					Account newAccount;
					
					if (accountTypeCB.getSelectedItem().equals("Spending account")) {
						newAccount = new SpendingAccount();
					} else {
						newAccount = new SavingAccount();
					}
					if (newSumaTF.getText().isEmpty()) {
						newAccount.setDeposit(oldAccount.getDeposit());
					} else {
						newAccount.setDeposit(Double.parseDouble(newSumaTF.getText()));
					}
					newAccount.setAccountNo(oldAccount.getAccountNo());
					
					banca.updateAccount(person, oldAccount, newAccount);
					banca.serializare();

					JOptionPane.showMessageDialog(null, "Account successfully updated!", "Succes", JOptionPane.PLAIN_MESSAGE);
					new OperatiiCont(banca);
				} catch (NumberFormatException e) {
					System.out.println("Problema input");
					new OperatiiCont(banca);
				} catch (NullPointerException e) {
					JOptionPane.showMessageDialog(null, "Person or account not found!", "Error", JOptionPane.ERROR_MESSAGE);
					new OperatiiCont(banca);
				} catch (IOException e) {
					System.out.println("Problema la serializare!");
					e.printStackTrace();
				} finally {
					dispose();
				}
			}
		});

		cancelBtn.addActionListener(new ActionListener() {
		
			public void actionPerformed(ActionEvent ae) {
				dispose();
				new OperatiiCont(banca);
			}
		});

		setResizable(false);
		setLocationRelativeTo(null);
		setVisible(true);
	}

}
