package frame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.*;

import proc.Account;
import proc.Bank;

public class Withdraw extends JFrame {

	private static final long serialVersionUID = 1L;

	private JLabel numarContLbl;
	private JLabel sumaLbl;
	
	private JTextField sumaTF;
	private JTextField numarContTF;

	private JButton withdrawBtn;
	private JButton cancelBtn;

	private JPanel panel;
	
	
	public Withdraw(final Bank banca) {
		panel = new JPanel();

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(335, 140);
		setTitle("Retragere numerar");

		panel.setLayout(null);
		
		add(panel);

		numarContLbl = new JLabel("Numar cont:");
	
		numarContLbl.setBounds(10, 10, 150, 20);
		panel.add(numarContLbl);
		
		numarContTF = new JTextField();
		numarContTF.setBounds(170, 10, 150, 20);
		panel.add(numarContTF);
		
		sumaLbl = new JLabel("Suma de bani:");
		
		sumaLbl.setBounds(10, 40, 150, 20);
		panel.add(sumaLbl);

		sumaTF = new JTextField();
		sumaTF.setBounds(170, 40, 150, 20);
		panel.add(sumaTF);

		withdrawBtn = new JButton("Retragere");
		
		withdrawBtn.setBounds(170, 70, 150, 30);
		panel.add(withdrawBtn);

		cancelBtn = new JButton("Cancel");
		
		cancelBtn.setBounds(10, 70, 150, 30);
		panel.add(cancelBtn);

		withdrawBtn.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent ae) {
				try {	
					
					
					long numarCont = Long.parseLong(numarContTF.getText());
					double suma = Double.parseDouble(sumaTF.getText());
					Account account = banca.findAccount(numarCont);
					if (account.withdrawMoney(suma)) {
						JOptionPane.showMessageDialog(null, "Withdraw succes!","Succes", JOptionPane.PLAIN_MESSAGE);
					} else {
						JOptionPane.showMessageDialog(null,"Withdraw problem!","Error", JOptionPane.ERROR_MESSAGE);
					}
				
					try {
						banca.serializare();
						
					} catch (IOException e) {
					System.out.println("Problema la serializare!");
						e.printStackTrace();
					}
					new Menu(banca);
				} catch (NumberFormatException e) {
				System.out.println("Problema input");
					new Menu(banca);
				} catch (NullPointerException e) {
					JOptionPane.showMessageDialog(null, "Account not found!", "Error", JOptionPane.ERROR_MESSAGE);
					new Menu(banca);
				} finally {
					dispose();
				}
			}
		});

		cancelBtn.addActionListener(new ActionListener() {
		
			public void actionPerformed(ActionEvent ae) {
				dispose();
				new Menu(banca);
			}
		});

		setResizable(false);
		setLocationRelativeTo(null);
		setVisible(true);
	}

}
