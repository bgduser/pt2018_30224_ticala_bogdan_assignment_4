package frame;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.*;
import proc.Bank;

public class DelPersoana extends JFrame {
	
	private static final long serialVersionUID = 1L;

	private JLabel idLbl;

	private JTextField idTF;

	private JButton delBtn;
	private JButton cancelBtn;

	private JPanel panel;
	
	
	public DelPersoana(final Bank banca){
		panel = new JPanel();

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(335, 110);
		setTitle("Stergere persoana");

		panel.setLayout(null);
		panel.setBackground(new Color(248, 248, 248));
		add(panel);

		idLbl = new JLabel("Persoana ID:");
	
		idLbl.setBounds(10, 10, 150, 20);
		panel.add(idLbl);

		idTF = new JTextField();
		idTF.setBounds(170, 10, 150, 20);
		panel.add(idTF);

		delBtn = new JButton("Sterge");

		delBtn.setBounds(170, 40, 150, 30);
		panel.add(delBtn);

		cancelBtn = new JButton("Cancel");
	
		cancelBtn.setBounds(10, 40, 150, 30);
		panel.add(cancelBtn);

		delBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				try {
					long id = Long.parseLong(idTF.getText());
					banca.deletePerson(banca.findPerson(id));
					banca.serializare();

					JOptionPane.showMessageDialog(null, "Person successfully deleted!", "Succes", JOptionPane.PLAIN_MESSAGE);
					new OperatiiPersoana(banca);
				} catch (NumberFormatException e) {
					System.out.println("Problema input");
					new OperatiiPersoana(banca);
				} catch (NullPointerException e) {
					JOptionPane.showMessageDialog(null, "Person not found!", "Error", JOptionPane.ERROR_MESSAGE);
					new OperatiiPersoana(banca);
				} catch (IOException e) {
					System.out.println("Problema la serializare!");
					e.printStackTrace();
				} finally {
					dispose();
				}
			}
		});

		cancelBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				dispose();
				new OperatiiPersoana(banca);
			}
		});

		setResizable(false);
		setLocationRelativeTo(null);
		setVisible(true);
	}

}
