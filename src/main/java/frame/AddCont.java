package frame;

	import java.awt.event.ActionEvent;
	import java.awt.event.ActionListener;
	import java.io.IOException;

	import javax.swing.*;

	import proc.Bank;
	import proc.SavingAccount;
	import proc.SpendingAccount;
	
public class AddCont extends JFrame {

		private static final long serialVersionUID = 1L;

		private JLabel idPersoanaLbl;
		private JLabel tipContLbl;

		private JTextField idPersoanaTF;
		


		@SuppressWarnings("rawtypes")
		
		private JComboBox tipContCB;

		private JButton cancelBtn;
		private JButton addBtn;

		private JPanel panel;

	
		
		
		public AddCont(final Bank banca) {
			
	
			panel = new JPanel();

			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			setSize(335, 170);
			setTitle("Adaugare cont");

			panel.setLayout(null);
			
			add(panel);

			idPersoanaLbl = new JLabel("ID persoana:");
			
			idPersoanaLbl.setBounds(10, 10, 150, 20);
			panel.add(idPersoanaLbl);

			idPersoanaTF = new JTextField();
			idPersoanaTF.setBounds(170, 10, 150, 20);
			panel.add(idPersoanaTF);

			tipContLbl = new JLabel("Tip cont");
		
			tipContLbl.setBounds(10, 40, 150, 20);
			panel.add(tipContLbl);

			String[] tipConts = { "Spending account", "Saving account" };
			tipContCB = new JComboBox<Object>(tipConts);
		
			tipContCB.setBounds(170, 40, 150, 20);
			panel.add(tipContCB);

			

			addBtn = new JButton("Adauga");
		
			addBtn.setBounds(170, 70, 150, 30);
			panel.add(addBtn);

			cancelBtn = new JButton("Cancel");
		
			cancelBtn.setBounds(10, 70, 150, 30);
			panel.add(cancelBtn);

			addBtn.addActionListener(new ActionListener() {
			
				public void actionPerformed(ActionEvent ae) {
					try {
						long idPersoana = Long.parseLong(idPersoanaTF.getText());
						
						if (tipContCB.getSelectedItem().equals("Spending account")) {
							banca.addAccount(banca.findPerson(idPersoana), new SpendingAccount(0));
						} else {
							banca.addAccount(banca.findPerson(idPersoana), new SavingAccount(0));
						}

						try {
							banca.serializare();
						} catch (IOException e) {
							System.out.println("Problema la serializare!");
							e.printStackTrace();
						}

						JOptionPane.showMessageDialog(null, "Account successfully added!", "Succes", JOptionPane.PLAIN_MESSAGE);
						new OperatiiCont(banca);
					} catch (NumberFormatException e) {
						System.out.println("Problema input");
						new OperatiiCont(banca);
					} catch (NullPointerException e) {
						JOptionPane.showMessageDialog(null, "Person not found!", "Error", JOptionPane.ERROR_MESSAGE);
						new OperatiiCont(banca);
					} finally {
						dispose();
					}
				}
			});

			cancelBtn.addActionListener(new ActionListener() {
			
				public void actionPerformed(ActionEvent ae) {
					dispose();
					new OperatiiCont(banca);
				}
			});

			setResizable(false);
			setLocationRelativeTo(null);
			setVisible(true);
		}

	}


