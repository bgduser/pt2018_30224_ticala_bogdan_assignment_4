package frame;

import proc.Bank;
import proc.Person;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Set;
import java.util.Vector;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class OperatiiPersoana extends JFrame {

	private static final long serialVersionUID = 1L;
	
	
	
	private JButton addBtn;
	private JButton editBtn;
	private JButton delBtn;
	private JButton backBtn;
	
	private JTable tabel;
	
	private JPanel panel;



	private Bank banca;

	
	public OperatiiPersoana(final Bank banca) {
		this.banca = banca;
		
		panel = new JPanel();
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(655, 470);
		setTitle("Operatii persoana");
		
		panel.setLayout(null);
		
		add(panel);
		
		addBtn = new JButton("Adaugare persoana");
		
		addBtn.setBounds(10, 10, 150, 30);
		panel.add(addBtn);
		
		editBtn = new JButton("Editare persoana");
		
		editBtn.setBounds(170, 10, 150, 30);
		panel.add(editBtn);
		
		delBtn = new JButton("Stergere persoana");
	
		delBtn.setBounds(330, 10, 150, 30);
		panel.add(delBtn);
		
		backBtn = new JButton("Back");

		backBtn.setBounds(490, 10, 150, 30);
		panel.add(backBtn);
		
		tabel = createTable();
		JScrollPane clientsSP = new JScrollPane(tabel);
		clientsSP.setBounds(10, 50, 630, 380);
		panel.add(clientsSP);
		
		backBtn.addActionListener(new ActionListener() {
		
			public void actionPerformed(ActionEvent ae) {
				dispose();
				new Menu(banca);
			}
		});
		
		addBtn.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent ae) {
				dispose();
				new AddPersoana(banca);
			}
		});
		
		editBtn.addActionListener(new ActionListener() {
		
			public void actionPerformed(ActionEvent ae) {
				//dispose();
				new EditPersoana(banca);
			}
		});
		
		delBtn.addActionListener(new ActionListener() {
		
			public void actionPerformed(ActionEvent ae) {
				dispose();
				new DelPersoana(banca);
			}
		});
		
		setResizable(false);
		setLocationRelativeTo(null);
		setVisible(true);
	}
	
	

	}


