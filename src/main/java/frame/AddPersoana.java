package frame;



import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.*;

import proc.Bank;
import proc.Person;

public class AddPersoana extends JFrame {

		private static final long serialVersionUID = 1L;

		private JLabel idLbl;
		private JLabel numeLbl;
		private JLabel telefonLbl;

		private JTextField idTF;
		private JTextField numeTF;
		private JTextField telefonTF;

		private JButton addBtn;
		private JButton cancelBtn;

		private JPanel panel;

		
		
		public AddPersoana(final Bank banca) {
			
			
			
			panel = new JPanel();

			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			setSize(335, 170);
			setTitle("Adaugare persoana");

			panel.setLayout(null);
	
			add(panel);

			idLbl = new JLabel("Person ID:");
			
			idLbl.setBounds(10, 10, 150, 20);
			panel.add(idLbl);

			idTF = new JTextField();
			idTF.setBounds(170, 10, 150, 20);
			panel.add(idTF);

			numeLbl = new JLabel("Nume:");
		
			numeLbl.setBounds(10, 40, 150, 20);
			panel.add(numeLbl);

			numeTF = new JTextField();
			numeTF.setBounds(170, 40, 150, 20);
			panel.add(numeTF);

			telefonLbl = new JLabel("Telefon:");
		
			telefonLbl.setBounds(10, 70, 150, 20);
			panel.add(telefonLbl);

			telefonTF = new JTextField();
			telefonTF.setBounds(170, 70, 150, 20);
			panel.add(telefonTF);

			addBtn = new JButton("Adaugare");
			
			addBtn.setBounds(170, 100, 150, 30);
			panel.add(addBtn);

			cancelBtn = new JButton("Cancel");
			
			cancelBtn.setBounds(10, 100, 150, 30);
			panel.add(cancelBtn);

			addBtn.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					try {
						long id = Long.parseLong(idTF.getText());
						String nume = numeTF.getText();
						String telefon = telefonTF.getText();
						Person persoana = new Person(id, nume, telefon);
						
						banca.addPerson(persoana);
						banca.serializare();

						JOptionPane.showMessageDialog(null, "Person added!", "Succes", JOptionPane.PLAIN_MESSAGE);
						new OperatiiPersoana(banca);
					} catch (NumberFormatException e) {
						System.out.println("Problema input");
						new OperatiiPersoana(banca);
					} catch (IOException e) {
						
						e.printStackTrace();
					} finally {
						dispose();
					}
				}
			});

			cancelBtn.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					dispose();
					new OperatiiPersoana(banca);
				}
			});

			setResizable(false);
			setLocationRelativeTo(null);
			setVisible(true);
		}

	}


