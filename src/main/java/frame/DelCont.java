package frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.*;

import proc.Bank;

public class DelCont extends JFrame {

	private static final long serialVersionUID = 1L;

	private JLabel personIdLbl;

	private JTextField personIdTF;

	private JButton deleteAccountBtn;
	private JButton cancelBtn;

	private JPanel panel;
	
	
	
	public DelCont(final Bank banca) {
		panel = new JPanel();

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(335, 140);
		setTitle("Stergere cont");

		panel.setLayout(null);
		
		add(panel);

		personIdLbl = new JLabel("Person ID:");
	
		personIdLbl.setBounds(10, 10, 150, 20);
		panel.add(personIdLbl);
		
		personIdTF = new JTextField();
		personIdTF.setBounds(170, 10, 150, 20);
		panel.add(personIdTF);
		
		
		deleteAccountBtn = new JButton("Delete account");
	
		deleteAccountBtn.setBounds(170, 40, 150, 30);
		panel.add(deleteAccountBtn);

		cancelBtn = new JButton("Cancel");
		
		cancelBtn.setBounds(10, 40, 150, 30);
		panel.add(cancelBtn);

		deleteAccountBtn.addActionListener(new ActionListener() {
		
			public void actionPerformed(ActionEvent ae) {
				try {
					long personId = Long.parseLong(personIdTF.getText());
				
					banca.deleteAccount(banca.findPerson(personId));
					banca.serializare();

					JOptionPane.showMessageDialog(null, "Account successfully deleted!", "Succes", JOptionPane.PLAIN_MESSAGE);
					new OperatiiCont(banca);
				} catch (NumberFormatException e) {
					System.out.println("Problema input");
					new OperatiiCont(banca);
				} catch (NullPointerException e) {
					JOptionPane.showMessageDialog(null, "Person not found!", "Error", JOptionPane.ERROR_MESSAGE);
					new OperatiiCont(banca);
				} catch (IOException e) {
					System.out.println("Problema la serializare!");
					e.printStackTrace();
				} finally {
					dispose();
				}
			}
		});

		cancelBtn.addActionListener(new ActionListener() {
		
			public void actionPerformed(ActionEvent ae) {
				dispose();
				new OperatiiCont(banca);
			}
		});

		setResizable(false);
		setLocationRelativeTo(null);
		setVisible(true);
	}

}
