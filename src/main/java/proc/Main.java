package proc;

import java.io.IOException;

import frame.Menu;

public class Main {
	public static void main(String[] args) throws IOException, ClassNotFoundException {
		
		Bank bank = new Bank();
		bank.deserializare();
		
		new Menu(bank);
		
	}
}
