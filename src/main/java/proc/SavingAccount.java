package proc;

public class SavingAccount extends Account {

	
	private static final long serialVersionUID = 1L;

	public SavingAccount(double suma) {
		
	}

	public SavingAccount() {
		
	}

	@Override
	public boolean addMoney(double suma) {
		if (getDeposit() != 0) {
			return false;
		} else {
			double dobanda = suma * 0.01;
			setDeposit(suma + dobanda);
			return true;
		}
	}

	@Override
	public boolean withdrawMoney(double suma) {
		if (getDeposit() == 0) {
			return false;
		} else if ((getDeposit() - suma) != 0) {
			return false;
		} else {
			setDeposit(0);

			return true;
		}
	}
	
	@Override
	public String toString() {
		return "Saving account" + super.toString();
	}
}
