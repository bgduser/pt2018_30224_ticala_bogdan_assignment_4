package proc;

import java.io.Serializable;

public abstract class Account implements Serializable {

	
	private static final long serialVersionUID = 1L;

	private double depozit;
	private long numarCont;
	
	public double getDeposit() {
		return depozit;
	}

	public long getAccountNo() {
		return numarCont;
	}
	public void setDeposit(double depozit) {
		this.depozit = depozit;
	}
	

	public void setAccountNo(long numarCont) {
		this.numarCont = numarCont;
	}
	
	

	public Account() {
		this.depozit = 0;
		numarCont = generateAccountNo();
	}
	
	public Account(double depozit) {
		this.depozit = depozit;
		numarCont = generateAccountNo();
	}
	public abstract boolean addMoney(double suma);
	public abstract boolean withdrawMoney(double suma);

}
