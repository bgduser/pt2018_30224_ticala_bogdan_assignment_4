package proc;

public class SpendingAccount extends Account {

	
	private static final long serialVersionUID = 1L;

	public SpendingAccount(double suma) {
	
	}

	public SpendingAccount() {
	
	}

	@Override
	public boolean addMoney(double suma) {
	
		setDeposit(getDeposit()+suma);
		return true;
	}

	@Override
	public boolean withdrawMoney(double suma) {
	
		if(getDeposit()< suma)
			return false;
		else {
			setDeposit(getDeposit()-suma);
			return true;
		}
		
	}
	
	@Override
	public String toString() {
		return "Spending account" + super.toString();
	}
	
	

}
