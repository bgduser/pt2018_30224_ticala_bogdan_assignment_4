package proc;

import static org.junit.Assert.*;

import org.junit.Test;


public class JUnitTest {


	@Test 
	public void testAddMoney() {
		
		Account account = new SpendingAccount(0);
		account.addMoney(100);
		assertTrue(account.getDeposit() == 100);
	}
	
	@Test
	public void testWithdrawMoney() {
		
		Account account = new SpendingAccount(10);
		account.withdrawMoney(10);
		assertTrue(account.getDeposit()== 0);
	}
	
	
}
