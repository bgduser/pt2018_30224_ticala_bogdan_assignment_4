package proc;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.*;


public class Bank implements BankProc, Serializable {

	private static final long serialVersionUID = 1L;
	

	Map<Person, Set<Account>> map;
	
	public Bank() {
		map = new HashMap<Person, Set<Account>>();
	}
	
	
	public Map<Person, Set<Account>> getMap() {
		return map;
	}



	public void addPerson(Person p) {
	
		assert p != null;
		assert !map.containsKey(p);
		assert wellFormed();
		int sizeBefore = map.size();
		
		map.put(p, new HashSet<Account>());
		
		int sizeAfter = map.size();
		assert sizeAfter == sizeBefore + 1;
		assert wellFormed();
		
	}

	public void deletePerson(Person p) {
		
		assert p != null;
		assert map.size() > 0;
		assert map.containsKey(p);
		assert wellFormed();
		int sizeBefore = map.size();
		
		map.remove(p);
		
		int sizeAfter = map.size();
		assert sizeAfter == sizeBefore - 1;
		assert wellFormed();
	}

	public void updatePerson(Person oldP, Person newP) {
		
		assert oldP != null;
		assert map.size() > 0;
		assert map.containsKey(oldP);
		assert wellFormed();
		
		Set<Account> accountSet = map.get(oldP);
		newP.setId(oldP.getId());
		map.remove(oldP);
		map.put(newP, accountSet);
		
		assert wellFormed();
		
	}

	public void addAccount(Person p, Account a) {
		
		assert p != null;
		assert a != null;
		assert map.containsKey(p);
		assert wellFormed();
		int sizeBefore = map.get(p).size();
		
		map.get(p).add(a);
	
		int sizeAfter = map.get(p).size();
		assert sizeAfter == sizeBefore + 1;
		assert wellFormed();
	}

	


	public void updateAccount(Person p, Account oldA, Account newA) {
		assert p != null;
		assert oldA != null;
		assert map.containsKey(p);
		assert map.get(p).size() > 0;
		assert map.get(p).contains(oldA);
		assert wellFormed();
		
		Set<Account> accountSet = map.get(p);
		accountSet.remove(oldA);
		accountSet.add(newA);
		
		assert wellFormed();
		
	}

	@SuppressWarnings("unchecked")
	public void deserializare() throws IOException, ClassNotFoundException {
		
		// Reading the object from a file
        FileInputStream file = new FileInputStream("bank.ser");
        ObjectInputStream in = new ObjectInputStream(file);
         
        // Method for deserialization of object
   
		map = (Map<Person, Set<Account>>) in.readObject();
		
         
        in.close();
        file.close();
        System.out.println("Object has been deserialized ");
		
	}
	public void serializare() throws IOException {
		
		 FileOutputStream file = new FileOutputStream("bank.ser");
         ObjectOutputStream out = new ObjectOutputStream(file);
          
         // Method for serialization of object
         out.writeObject(map);
          
         out.close();
          
         System.out.println("Object has been serialized");
	}

	public Person findPerson(long id) {
		
	
		 Set<Person> key=map.keySet();
         for (Person p : key) 
        	 if (p.getId()== id)
        		 return p;
         
         throw new NullPointerException();
	
	}

	public Account findAccount(long contNo) {
		
		Set<Person> key = map.keySet();
		
		for(Person p : key) {
			Set<Account> cont = (Set<Account>) map.get(p);
			for(Account a : cont)
				if(a.getAccountNo()==contNo)
					return a;
		}
		throw new NullPointerException();
	}


	
}
