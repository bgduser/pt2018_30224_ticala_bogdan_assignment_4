package proc;

public interface BankProc {
	
	void addPerson(Person p);
	/*
	 * parametru person
	 * preconditie person != null
	 * preconditie map.containsKey(person) == false
	 * postconditie map.size() == map.size()@pre + 1
	 */
	
	void deletePerson(Person p);
	/*
	 * param oldPerson
	 * param newPerson
	 * pre oldPerson != null
	 * pre map.size() > 0
	 * 
	 * returns true if and only if this map contains a mapping for a key oldPerson
	 * pre map.contains(oldPerson) == true
	 */
	
	void updatePerson(Person oldP, Person newP);
	
	
	void addAccount(Person p, Account a);
	
	
	void deleteAccount(Person p);
	

	void updateAccount(Person p, Account oldA, Account newA);



}
