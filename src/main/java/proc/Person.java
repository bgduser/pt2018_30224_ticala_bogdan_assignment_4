package proc;

import java.io.Serializable;


public class Person implements Serializable {

	private static final long serialVersionUID = 1L;

	private long id;
	private String nume;
	private String telefon;

	public Person() {
		
	}
	
	public Person(long id, String nume, String telefon) {
		this.id = id;
		this.nume = nume;
		this.telefon = telefon;
	}


	public long getId() {
		return id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		if (id != other.id)
			return false;
		return true;
	}

	public String getNume() {
		return nume;
	}

	public String getTelefon() {
		return telefon;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public void setNume(String nume) {
		this.nume = nume;
	}
	
	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}
	
	@Override
	public String toString() {
		return "Person[ID = " + id + ", nume = " + nume + ", telefon = " + telefon + "]";
	}



	

}
